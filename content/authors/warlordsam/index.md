---
# Display name
title: warlordsam

# Username (this should match the folder name)
authors:
- warlordsam

# Is this the primary user of the site?
superuser: false

# Role/position
role: GSoC'22 Contributor


# Short bio (displayed in user profile at end of posts)
bio: Hey, My name is Pratik Lagaskar (alias I use - warlordsam). I am a Second Year Undergraduate student, currently studying Electronics and Telecommunications Engineering with interest in cybersecurity and open-source.

# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.
social:
- icon: twitter
  icon_pack: fab
  link: https://twitter.com/warlordsam077
- icon: github
  icon_pack: fab
  link: https://github.com/WarlordSam07
- icon: linkedin
  icon_pack: fab
  link: https://www.linkedin.com/in/pratik-lagaskar-a8747b20a/
# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
# user_groups:
#- Developers
#- Designers

---
