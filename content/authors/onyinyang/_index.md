---
# Display name
title: onyinyang

# Username (this should match the folder name)
authors:
- onyinyang

# Is this the primary user of the site?
superuser: false

# Role/position
role: Censorship Circumvention Researcher


# Short bio (displayed in user profile at end of posts)
bio: Recent graduate (MMath) of the CrySP lab at the University of Waterloo. Now researching UDP based pluggable transports with traffic obfuscation at LEAP.

# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.
social:
- icon: twitter
  icon_pack: fab
  link: https://twitter.com/algorithknit
- icon: link
  icon_pack: fas
  link: https://onyiny-ang.github.io/
# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
# user_groups:
#- Developers
#- Designers

---
