---
title: "GSOC invitation"
subtitle: "Contribution and application ideas"
summary: A couple of starting ideas for GSOC applicants
authors:
- cyberta
- kwadronaut
tags:
- gsoc
- code
- DNS block
categories:
- vpn
date: "2022-02-21T00:00:00Z"
featured: false
draft: false
---

Want to spend time on cool tech that actually helps people? Getting paid by Google for your contributions? Then read on, first some guidelines, then move on to our [Ideas List](https://leap.se/software/wishlist). We think they're right sized for the time frame, but if you can come up with other ideas: you're most welcome!

## HOW TO APPLY
Short application checklist:

1. Talk with your prospective mentors about what they expect of GSoC applicants and get help from them to refine your project ideas. Listening to your mentors' recommendations is very important at this stage! A small patch for the client or server goes a long way. Usually we expect GSoC contributors to fix a bug and have made a pull request (or equivalent). Your code doesn't have to be accepted and merged, but it does have to be visible to the public and it does have to be your own work (mentor help is ok, code you didn't write is not).
1. Write your application, get someo feedback. All applications must go through Google's application system; we can't accept any application unless it is submitted there.
1. Use a descriptive title in Google's system. Good example: "Implement proverd>client notification" Bad example: "My gsoc project."
1.Make it easy for your mentors to give you feedback. Think of how you send proposals: hidden within Google docs or a PDF means you're most likely not going to get accepted. Make sure we can get back to you, include an e-mail adress for example.
1. Submit your application to Google before the deadline. We actually recommend you submit a few days early in case you have internet problems or the system is down. Google does not extend this deadline, so it's best to be prepared early! You can edit your application up until the system closes.

### Tip
Communication is probably the most important part of the application process. Talk to the mentors and other developers, listen when they give you advice, and demonstrate that you've understood by incorporating their feedback into what you're proposing. We reject a lot of applicants who haven't listened to mentor feedback. If your mentors tell you that a project idea won't work for them, you're probably not going to get accepted unless you change it.

 
### What goes in an application?
An ideal application will contain 5 things:
1. A descriptive title including the name (if this is missing, your application may be rejected!)
1. AInformation about you, including contact information.
1. ALink to a code contribution you have made.  Usually this is a link to a pull request.
1. AInformation about your proposed project. This should be fairly detailed and include a timeline.
1. AInformation about other commitments that might affect your ability to work during the GSoC period. (exams, classes, holidays, other jobs, weddings, etc.) We can work around a lot of things, but it helps to know in advance.

## Need ideas?  
Our [Ideas List](https://leap.se/software/wishlist) is a good start!

### More ideas?

There are more clouds in the sky than ideas and wishes we would like to see integrate in the near future. If you're motivated and creative, write your proposal, as sketched out above, and submit it. We're open for (almost) anything.

info@leap.se is where you need to send it all

When you want to help out:

* Code & Bugtracker: <https://0xacab.org/leap>
* ♥ donation: <https://leap.se/en/about-us/donate>
* Desktop translations: https://www.transifex.com/otf/bitmask/RiseupVPN/
* Android translations: https://www.transifex.com/otf/bitmask-android/dashboard/
* Mailinglist: leap-discuss@lists.riseup.net
* Chat: ircs://irc.libera.chat/#leap if you don't have an irc client, you can use a gateway like [Matrix](https://about.riot.im/)

Thanks to python-gsoc for some ideas
