---
title: "GSOC wish list"
subtitle: "Contribution and application ideas"
summary: A couple of starting ideas for GSOC applicants
authors:
- cyberta
- kwadronaut
tags:
- gsoc
- code
- DNS block
- ideas
categories:
- vpn
date: "2022-02-21T00:00:00Z"
featured: false
draft: false
---

We've given 2 fleshed out examples, a whishlist, but there are many more areas where there are missing features, UX improvements, stability or other things are possible. We're hinting at somet things at the bottom. If you want to apply for GSOC, don't forget to check the [invitation](https://leap.se/software/gsoc/). If you want to get in contact with the mentors, write a mail or hang out in our chat.

## Need ideas?

## Client-side Tracking-, Malware and Ad-Blocker

* Description:  
Our users, often in precarious situations, need to be better protected from malware, tracking and ads. There are different possible approaches: client side or both server- and client side. Since our tech stack includes the cross-platform language golang, this will be the language of our choice. 
The UI components will be implemented depending on the platform. Whereas our Desktop client is developed with QT5, the Android UI is implemented in Java and Android-specific XML. For the first iteration, a simple feature switch in the settings along with an explanation what the feature does is sufficient. The blocking lists will be pre-shipped with each release so that no further server communication is required. A second iteration can implement an update mechanism, but that would probably too much for GSOC.
There are some existing open-source Android apps using the VPN functionality to filter out unwanted DNS requests without offering an actual VPN service. One example that is adapted for the same upstream software as our client Bitmask Android is based on is called personaldnsfilter (<https://www.zenz-solutions.de/openvpn-for-personaldnsfilter/>). Another one is called <https://github.com/julian-klode/dns66/>. Even though these apps are written in Java and thus aren't applicable for our Desktop clients, they can serve for inspiration.

* Outcome  
The final client side solution should ideally run on all currently supported platforms: Android, Windows, Mac and Linux and should be potentially useable in our future iOS client as well. 
The solution should be packaged as a self-contained go library that is able to read packages from a given file-desciptor, identify DNS-requests and block those (reply with REFUSE) whose target IPs are listed in our blocking lists. All other packages should be forwarded to another file-descriptor which the openvpn client consumes and processes.

* Required skills  
Golang and  QT experience, be self learning and not be shy to ask questions. Git is a must.

* Possible mentors  
cyberta and kwadronaut

* Expected size 175 hours

* Difficulty medium


## Server-side Tracking-, Malware and Ad-Blocker

* Description   
Just as the first idea: block malware, trackers and ads on a DNS-level. Need to have a gateway advertise it's filtering capabilities. Need to change the API-json, adapt the DNS-service container and keep the lists to be blocked up-to-date. Need to research how clients react to different DNS replies, for example letting them time-out is a bad idea, REFUSED probably is though. That way they get a fast failure, time-outs degrade the user experience. OpenVPN doesn't know which DNS server it should tell the clients, so you would need a secondary OVPN process, listening on different ports. For example shapeshifter wouldn't be able to handle a second OpenVPN process. Therefore bridges or circumvention conflicts with DNS-filtering **AND** without filtering. An additional task to make it all smooth: measuring impact on load balancer, and setup a testing environment.

* Outcome    
Gateway support for both filtered and unfiltered DNS requests would be the ideal outcome. Acceptable but very minimal is to have gateways to be unfiltered **OR** filtered. There's an in between as well, expanding on the previous one: have both filtered and unfiltered on a single gateway where 1 OpenVPN server listens on a different port and uses a different DNS-service in the background. To achieve that, we would also need to adapt the eip.json that clients parse to know which gateways can be used.

* Required skills
  * load balancer: go
  * server platform: ansible  
  * git

* Possible mentors  
kwadronaut cyberta and micah

* Expected size 350 hours
* Difficutlty medium to hard

## UX parity across the platforms

* Description  
We spend 2021 working on UX with Simply Secure and the work is reflected in the desktop client. Now we need to achieve UX parity between android and desktop. Doing more testing with for example rtl and providing automated screenshots to the translators (volunteers).

* Outcome  
Getting the same user experience and no suprises for users that use different platforms.

* Required skills
  * QT
  * Java
  * git 

* Possible mentors
cyberta and mcnair

* Expected size 175 hours
* Difficutlty easy to medium 


### More ideas?

There are more clouds in the sky than ideas and wishes we would like to see integrate in the near future. If you're motivated and creative, write your proposal, as sketched out above, and  submit it. We're open for (almost) anything.

Some unfleshed ones:
* add a service status page for users: which gateways are online, health of DNS resolvers, is the GW load balancer online, API endpoint, scheduled maintenance -
* interface for admins to push notifications to users
* add to the current dashboards for provider admins more health information, for example about the gateway balancer, amount of connected users, failed connections,...
* add ESNI support, together with TLS 1.3 everywhere.

info@leap.se is where you need to send it all

When you want to help out:

* Code & Bugtracker: <https://0xacab.org/leap>
* ♥ donation: <https://leap.se/en/about-us/donate>
* Desktop translations: https://www.transifex.com/otf/bitmask/RiseupVPN/
* Android translations: https://www.transifex.com/otf/bitmask-android/dashboard/
* Mailinglist: leap-discuss@lists.riseup.net
* Chat: ircs://irc.libera.chat/#leap if you don't have an irc client, you can use a gateway like [Matrix](https://about.riot.im/)

Thanks to python-gsoc for some ideas
