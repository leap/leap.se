---
title: "FAQ"
summary: "Answers to often asked questions"
subtitle: "Your question is probably here"
authors:
- kwadronaut
tags:
- LEAP
- About
- Bitmask
categories:
- vpn
- faq
date: "2022-03-26T00:00:00Z"
lastmod: "2022-03-17T10:45:02z"
view: 1
bg_image: featured.png
featured: false
draft: false
---


## Answers and Questions

* [What are LEAP, Bitmask, CalyxVPN, RiseupVPN, …?](#Whats_bitmask)
* [Why does this website show me Russian content?](#Alien_content)
* [How can I contribute?](#contribute)
* [Where is the code?](#where_is_the_code)
* [Who are you?](#Who)

## What are LEAP, Bitmask, CalyxVPN, RiseupVPN, …? {#Whats_bitmask}

* LEAP is the organisation that does the development. It stands for LEAP
Encryption Access Project, a [recursive
acronym](https://en.wikipedia.org/wiki/Recursive_acronym)
* Bitmask is the name of our Android app. You can use it with any service provider
that is using the LEAP software or even having the same setup.
* [SurVPN](https://survpn.net/), [CalyxVPN](https://f-droid.org/en/packages/org.calyxinstitute.vpn/),
  [RiseupVPN](https://riseup.net/vpn): those are all service providers that use
the same platform and code that we build. Both Calyx and Riseup have their own
'branded' apps, own logo and looks. We think they're all nice groups, but keep
in mind that they have a different audience, check them out first. Or setup your
own service provider!


## Why does this website show me Russian, Spanish,… instead of something else?{#Alien_content}

Short version: the page you're visiting thinks you are there, even when you
choose a different location (Miami, Amsterdam, New York…). They use the gateway
IP to determine this.

Longer version: IPs are distributed by several [Regional Internet
Registries](https://en.wikipedia.org/wiki/Regional_Internet_registry), that's
one way to decide what language people probably speak. But Youtube, Facebook,
Amazon and many others use several techniques and build their own databases
mapping location, language and IP address. Your browser sends preferred locales,
people have cookies or are logged in and have their preferences saved or their
phone sends a location. Even the content people consume. Imagine reading your
local newspaper online. When there is a significant amount of people that are
having similar browsing behavior *and* all using the same IP from the VPN end
point those websites will decide that they serve their pages in odds and ends
language. Here's [a bug report](https://0xacab.org/leap/bitmask-vpn/-/issues/642)
 that demonstrates this.


## How can I contribute?{#contribute}

* [Donate](https://leap.se/donate/)
* [Code & bugs and fixes:](https://0xacab.org/leap/) we're using a Gitlab
  instance where you can bestow this project.
* [Translations and
  localization:](https://wiki.localizationlab.org/index.php/Bitmask) Transifex
is where we're gathering translations by volunteers like you. The
Localization Lab helps us, they've got a short intro on how to contribute.

## Show me the code!{#where_is_the_code}

It's living at a [Gitlab](https://0xacab.org/leap). There are a bunch of
repositories, like for [Android](https://0xacab.org/leap/bitmask_android),
[desktop](https://0xacab.org/leap/bitmask-vpn) or the server side, called
[lilypad](https://0xacab.org/leap/container-platform/lilypad). Changes for the
[website](https://0xacab.org/leap/leap.se) are welcome as well.

## Who are you?{#Who}

We've been a distributed team since the very beginning.
