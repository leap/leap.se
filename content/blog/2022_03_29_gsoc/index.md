---
title: "Google summer of code: join us!"
slug: apply-with-us-to-gsoc-2022
summary: "GSOC - check it out and apply"
subtitle: "Why don't you build an ad-blocker into the VPN?"
authors:
- kwadronaut
tags:
- GSOC
categories:
- vpn
- dev
- outreach
date: "2022-03-26T00:00:00Z"
lastmod: "2022-03-29T00:00:00Z"
featured: true
draft: false
---

## Join us to improve Bitamsk and RiseupVPN

Google accepted us as an organization for their "Summer of Code." It's a
world-wide program (check
[eligibility](https://summerofcode.withgoogle.com/rules). Thanks to that, we're
able to invite people to work on discreet pieces that can improve a lot of
different parts of the VPN, whether on the platform side, Android App or
elsewhere. These contributions would be very helpful and it's a great possibility to
start learning more about this project, code, how and where to use it… One of
the playing rules is that you are contributing already. Don't worry, it's a low
barrier, meaningful bugs, un-merged code: everything counts.

## Where do I start? 

A very first step, if you haven't done this yet, is to install the client
software, try it out, read up on what the providers try to do. RiseupVPN or
Bitmask are a good start to get familiar with it. Afterwards, it's time to
explore the [bugtracker](https://0xacab.org/leap/), see if there's something easy to work on,
propose something else. You should definitely check out the [ideas
list](https://leap.se/software/wishlist/) and [our
expectations](https://leap.se/software/gsoc/).

## It's a once in a blue moon opportunity to have Google let you build an ad-blocker!


Image by [anhdr](https://anhdr.es/2020/05/22/artwork-for-tails-homepage/)
