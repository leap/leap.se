---
title: "Building a Censorship Resistant Pluggable Transport for UDP"
slug: "building-a-censorship-resistant-pt-for-udp"
summary: "We are on a quest to develop a new UDP based Pluggable Transport for censorship resistance"
authors:
- onyinyang
tags:
- pluggable-transport
- OpenVPN
- UDP
- turbotunnel
categories:
- vpn
- dev
date: "2022-05-22T00:00:00Z"
lastmod: "2022-05-22T00:00:00Z"
featured: true
draft: false
---

## Vulnerable VPNS, Promising Protocols

VPNs have become a popular tool for evading surveillance and circumventing censorship. [OpenVPN](https://openvpn.net/) is a free and open-source VPN system that many popular VPNs used for censorship circumvention are built on. Despite many of these VPN services promising security and unidentifiability to users of their services, a new paper by [Xue et al.](https://www.usenix.org/conference/usenixsecurity22/presentation/xue-diwen) shows that OpenVPN based VPNs have fingerprintable features, such as unique opcodes and ACK sequences, and that many commercial VPNs fail to implement obfuscation that prevent these unique features from being identified.

To mitigate this fingerprintability, Xue et al. recommend short-term mitigations such as ensuring that obfuscated OpenVPN and obfuscation servers are not co-located or easily linked to OpenVPN instances, that VPN providers use random padding rather than static padding for all obfuscated services, and that servers respond less predictably to failed handshake attempts.

Along with VPN specific vulnerabilities is the ongoing targetting of connections deemed suspicious by censors. Many censorship circumvention tools used today depend, to some extent, on TLS which requires the reliability of TCP-based connections. In response, censors have developed sophisticated methods such as [deep packet inspection](https://www.cs.umd.edu/class/fall2018/cmsc818O/papers/sok-censorship.pdf), [active probing](https://ensa.fi/active-probing/imc2015.pdf) and other [attacks on the TCP state](https://www.cs.ucr.edu/~zhiyunq/pub/imc17_censorship_tcp.pdf) to surveil and block TCP-based connections.

With the development of HTTP/3 protocols such as QUIC, KCP and SCTP, that
provide reliability and security for UDP streams, there has
been growing interest in exploring UDP based censorship resistance tools and
censors' existing and hypothesized strategies to block them. HTTP/3 protocols provide an interesting avenue to explore and develop new modes of censorship circumvention. In a study by [Elmenhorst et al.](https://dl.acm.org/doi/pdf/10.1145/3487552.3487836), censors in Iran, India and China were observed to have an almost 1:1 mapping in handshake timeout errors between TCP-based connections and their QUIC-based counterparts. While this seems to indicate that SNI blocking applies to both TCP and QUIC, Elmenhorst et al. note that the QUIC packets that carry this information are protected by connection and version-specific keys, which do not prevent decryption but do make QUIC-SNI blocking less efficient. Indeed, in a report by [OONI](https://ooni.org/post/2022-http3-measurements-paper/) QUIC-SNI blocking was recorded only very rarely. Other techniques that are effective for blocking TCP and TLS connections were seen to be ineffective against QUIC connections. In support of their study, Elmenhorst provides several additional [insights on HTTP/3 censorship methods](https://github.com/kelmenhorst/quic-censorship/blob/main/document.md), [potential strategies for exploiting QUIC censors](https://github.com/kelmenhorst/quic-censorship/blob/main/evade.md) and a tool for [testing HTTP/3 connections](https://github.com/kelmenhorst/quic-censorship/blob/main/browsers.md).


Pluggable transports are an obvious area for innovation with HTTP/3 protocols. [Hogan](https://dspace.mit.edu/handle/1721.1/128590) gives a detailed analysis of the built in censorship resistant properties of QUIC that have already been used to implement new pluggable transports ([hysteria](https://github.com/HyNetwork/hysteria), [v2ray](https://github.com/v2ray/v2ray-core)). To our knowledge, other UDP based transports such as KCP and SCTP are less well explored, though may also be suitable for censorship resistance. Fifield's
[Turbotunnel](https://www.bamsoftware.com/papers/turbotunnel/) which was presented as a design pattern for circumvention protocols, has acted as a blueprint for taking advantage of the reliability/session layers provided by HTTP/3 protocols like KCP and QUIC and adding an obfuscation layer on top.

## The Path to Building Robust UDP-based Pluggable Transports

At LEAP, we are working on a UDP-based pluggable transports with traffic obfuscation and are
[currently hiring for a Go Developer](/blog/wanted-go-app-developer-to-fight-censorship) to help support this effort. In the coming months we will be trying to answer to the following questions:

1) What are the most interesting avenues to explore with respect to VPN obfuscation and UDP-based pluggable transports?
2) How fingerprintable/blockable is UDP encapsulation and how can we make it more difficult for censors to identify and block?
3) How can the resilience of UDP-based Pluggable Transports be improved in general, for uses beyond hiding OpenVPN traffic.
4) Who else is doing work in this space? What insights can be gained from parallel or related efforts?

We will be posting regular blog posts and are hoping that through this blog and through talking with other researchers and organizations that may be working on similar things, we can collectively share insights and best practices to avoid reinventing the wheel while developing a diverse collection of tools for robust and effective censorship resistance.

Keep a look out here for the next post and please contact us via IRC or at info@leap.se for questions to share ideas or otherwise collaborate further.


