---
title: A warm welcome to Lilypad
title: hello-lilypad
summary: Introduction to our new platform, the server side
subtitle: A contemporary server platform for all the VPNs
authors:
- kwadronaut
tags:
- Platform
- introduction
- Lilypad
categories:
- platform
date: "2021-08-31T00:00:00Z"
lastmod: "2021-08-31T00:00:00Z"
featured: false
draft: false
---

## Lilypad: a new contemporary platform for our servers 

Coming soon to the background of your favorite providers.... the first release of our new rewrite of the LEAP Platform! To celebrate, we've given it a new name: [“Lilypad”](https://0xacab.org/leap/container-platform/lilypad). It feels great to clean out the old bits, removing many unused parts, re-think everything from scratch and to build on a new paradigm! 

There are so many improvements, its hard to list them all, some highlights:

* more up-to-date security (ciphers, software updates…)
* better gateway selection, if you can't yet choose gateways you should update the app
* expanded connectivity options for a faster gateway future (IPv6, UDP support)
* read-only, container-based architecture
* automated Lets Encrypt integration
* modern monitoring/alerting/logging and analysis stack
* more censorship evasion techniques… 
* removal or replacement of many components for an overall cleaner system

One of the things people will enjoy is a better understanding of gateway load. When you see the gateway congestion you could switch to a different gateway, be more patient or consider a donation so another gateway can be added. 

At this point we would like to invite people to [try it out](https://0xacab.org/leap/container-platform/lilypad), create [pull requests](https://0xacab.org/leap/container-platform/lilypad/-/merge_requests), or [comment on it](https://0xacab.org/leap/container-platform/lilypad/-/issues).

It took us a while to get there and must thank everyone that made this possible: 
- Prototype Fund 
- Autistici/Inventati for the [float](https://git.autistici.org/ai3/float) toolkit, which lilypad is leaning heavily on.
- Riseup Labs 
- Individuals for advice, code or comments 
- to all the pieces that we no longer need, thank you! (puppet, apache,
  passenger, couchdb, nagios, shorewall, soledad, unbound, and leap_cli, and
  many others)
- everyone around the LEAP ecosystem! 
