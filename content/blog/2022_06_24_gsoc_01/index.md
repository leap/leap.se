---
title: "Introducing VPN-Hole, a Go library for blocking ads and malware"
slug: "go-library-to-block-ads"
summary: "Getting started with VPN-Hole, a self-contained GoLang library for blocking ads on the client-side that will be integrated with various cross-platform applications at LEAP."
authors:
- warlordsam
tags:
- ad-blockers
- dns-sinkhole
- pi-hole
- golang
categories:
- dev
- GSoC
date: "2022-06-24T00:00:00Z"
lastmod: "2022-06-24T00:00:00Z"
featured: true
draft: false
---

## GSoC'22 Series #1: VPN-Hole, the beginning

Hello everyone!

Sharing the first introductory post about the beginning of [GSoC-2022](https://summerofcode.withgoogle.com/). Hopefully, this series of posts will ensure that I introduce and keep a track of my journey with LEAP Encryption Access Project under the guidance of the awesome mentors - [Cyberta](https://0xacab.org/cyberta) and [Kwadronaut](https://leap.se/author/kwadronaut/).

## What is GSoC?
Feel free to skip this section if you already know about it. GSoC or Google Summer of Code is an annual program administered by Google where large number of students/contributors from around the world are introduced to various open source organizations. Selected GSoC contributors work for 12-18 weeks on different open-source projects for their respective organizations as per their proposal or org's wishlist. Every contributor gets paid a stipend of $1500 or more by Google, depending on their country of residence.

## Mapping the timeline till now:
I will try to summarize the events in brief.

[1] Org Selection: Being a cybersecurity and privacy enthusiast, I chose to contribute to LEAP Encryption Access Project; as their vision somewhat aligned with mine. The idea that they had proposed in their wishlist was also something that caught my interest. Upon checking the requirements and tech-stack involved,  I felt confident about contributing to this awesome project. I then submitted my proposal showing my interest and got a callback for further process. Received my GSoC results on 20th of May and thus I got selected as a contributor at LEAP for the year-2022.  

[2] Community Bonding Period: Right from the first interactions, my mentors were really helpful in guiding me through joining the community and providing valuable inputs in everything I asked. Everyone in the community (IRC) was so patient, polite and helpful whenever I needed their help. This gave me a sense of belonging in the LEAP community. That was a really nice start for me.

[3] Coding period begins: The official coding period has just begun from 13th of June. My tasks through-out the project have been mapped out for me. A separate repository for the library has also been set-up. I have also explored the entirety of LEAP's repositories and got familiar with them. Testing integrations of sample Go-libraries with existing applications is also done till now and further work on main library is in progress. Further updates on implementation will be shared in the next blog. 

## What is this series about?

In this series of blog posts, I mainly plan to introduce you to 'VPN-Hole' (christened by kwadronaut) and provide a walk-through of implementation and integration parts. There will be total 3 blog posts in this series- the beginning, implemenation and wrapping up. This post will mainly focus on motivation, idea and proposed solution for fighting against ads,trackers and malwares. So, this will be more of a non-technical overview of the project.

### TL;DR for this entry

Generally users dislike ads and trackers, and for a good reason. They should have option/freedom to choose whether they want see the ads or not. VPN-hole provides them with exactly that. It is a self-contained library written in Go; which will be integrated with existing applications at LEAP. It will help us to minimize the occurences of third-party ads on websites and various applications in android,etc. 

### Why should we care about blocking ads and trackers?

Ads or advertisements, targeted or general, are always an annoyance for an end user. Nowadays, we get to see ads embedded in most of the websites or applications we visit. Sometimes, they just are just annoyance but they can also be a reason for redirecting a user to suspicious/malicious websites that are potentially dangerous. Ever wondered what happens when you load a webpage? - Whenever a user visits a site; suppose - example.com, the content that is supposed to be served to user is loaded - along with that the requests for embedded urls of ad-services are also served. Most common reason for ads on webpages is 'Monetization'. Running websites/services takes a lot of money and thus, the ads come into the play. But how is it affecting the surfing experience for an end user?

Ads, trackers and malwares:

- they slow down a website (loading speed decreases).
- affects user's experience by creating annoyance.
- when served as 'targeted ads'- can breach the privacy of users.
- are potentially harmful (redirecting to suspicious websites).
- trackers can collect user's data as they move through the web.

These are just few reasons why everyone dislikes ads and trackers. So, we have established that ads are bad and user's should have control over whether to see them or not. Here at LEAP, we believe that - "Privacy should not be a privilege". So, to protect our users' privacy and at the same time giving them freedom from the ads and trackers - VPN-hole (name might change) is introduced.

### What is VPN-hole?

VPN-Hole is a library written in [GoLang](https://go.dev/); which is inspired by the famous [pi-hole](https://pi-hole.net/) project. Pi-hole project had its own limitations when it came to implementing it across various platforms. It is also too complex to install for a user who is not into tecnhical stuff. Keeping in mind the usability aspects of installation, cross-platform usage and better experience; VPN-Hole was introduced. Pi-Hole does not run on mobile devices, in contrast to that vpn-hole has its own uniqueness - as it runs on mobile devices as well. Main reason to write the library using Go was to make it available for different platforms and operating systems.

VPN-Hole, a DNS-level client-side blocker, is based on the most effective method of ad-blocking; known as - [DNS Sinkholing](https://en.wikipedia.org/wiki/DNS_sinkhole). The general idea here is that we need to block domains that are known to serve ads, trackers and malwares using a pre-shipped blocklist. The blocking would be done locally on the client-side according to the curated blacklist. VPN-hole will run a custom DNS server that will selectively block the unwanted domains by replying NXDomain(Non-Existent Domain) to the client or handing out non-routable addresses for blocked domains.

Using DNS for content blocking has its own advantages and disadvantages. First-Party ads like that of YouTube videos cannot be blocked as they are hosted on the same domain as legitimate videos. The web pages might have broken frames and ad-placeholders. These things cannot be avoided or worked around and we can do nothing about it. But on the same note, users will have smoother surfing experience and we will be providing a way to protect privacy and security as well.

Most of the ad-blockers we see around us are generally in the form of web-extensions or standalone applications. But, VPN-Hole would be integrated into VPN applications at LEAP. The library would be compiled for different architectures/OSes using GoMobile and GoBind. Language bindings will be generated which can be called from Java; which is used in [bitmask_android](https://0xacab.org/leap/bitmask_android). User interface for existing android VPN application will also be developed with feature of toggling enable/disable DNS Filtering.  


## Next entry on this GSoC'22 series: 

In the next blog post, we will have a detailed walk-through of implementation of VPN-Hole; which will serve as a brief documentation or guide for future contributors, developers or anyone looking to get involved with this project. We will also look into the integration of the library with [bitmask_android](https://0xacab.org/leap/bitmask_android).

So, please keep a look out for the next post and provide your valuable inputs and suggestions at info (at) leap (dot) se or via IRC.
